FROM mysql:5.7.19

EXPOSE 3306

ENV MYSQL_ROOT_PASSWORD root

COPY 01-schema.sql /docker-entrypoint-initdb.d

COPY 02-schema-data.sql /docker-entrypoint-initdb.d

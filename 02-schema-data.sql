USE media;

INSERT creator (name, creator_type)
VALUES ('rimsa', 'actress');

INSERT creator (name, creator_type)
VALUES ('ivaylo', 'musician');

INSERT creator (name, creator_type)
VALUES ('bob', 'actress');

INSERT creator (name, creator_type)
VALUES ('tim', 'musician');

INSERT creator (name, creator_type)
VALUES ('sara', 'musician');


INSERT purchase_info (date_time, amount_paid, url)
VALUES (now(), 150, 'http://spotify.com');

INSERT purchase_info (date_time, amount_paid, url)
VALUES (now(), 560, 'http://apple.com');

INSERT purchase_info (date_time, amount_paid, url)
VALUES ('2010-7-01', 29, 'http://googleplay.com');

INSERT purchase_info (shop, date_time, amount_paid)
VALUES ('HMV',now(), 15);

INSERT purchase_info (shop, date_time, amount_paid)
VALUES ('Google', '2013-1-01', 11);


INSERT media_info (media_type, title, year, genre, blurb, creator_id, purchase_id)
VALUES ('movie', 'Twilight', 2008, 'romance','Vampires...',1,1);

INSERT media_info (media_type, title, year, genre, blurb, creator_id, purchase_id)
VALUES ('music', 'If I were a boy', 2008, 'pop','queen b wannabe',2,2);

INSERT media_info (media_type, title, year, genre, blurb, creator_id, purchase_id)
VALUES ('movie', 'Harry Potter', 2008, 'fantasy','Wizards!',3,3);

INSERT media_info (media_type, title, year, genre, blurb, creator_id, purchase_id)
VALUES ('music', 'White horse', 2008, 'country','Taylors breakups',4,4);

INSERT media_info (media_type, title, year, genre, blurb, creator_id, purchase_id)
VALUES ('music', 'Purple rain', 2008, 'old','prince song',5,5);

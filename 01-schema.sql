

CREATE DATABASE media;

use media;

USE media;

CREATE TABLE creator (
	creator_id INT AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(200) NOT NULL,
	creator_type VARCHAR(20) NOT NULL
);


CREATE TABLE purchase_info (
	purchase_id INT AUTO_INCREMENT PRIMARY KEY,
	shop VARCHAR(100),
	date_time datetime NOT NULL,
	amount_paid FLOAT NOT NULL,
	url VARCHAR(200)
);

CREATE TABLE media_info (
	id INT AUTO_INCREMENT PRIMARY KEY,
	media_type VARCHAR(10) NOT NULL,
	title VARCHAR(30) NOT NULL,
	year INT NOT NULL,
	genre VARCHAR(50) NOT NULL,
	blurb VARCHAR (200) NOT NULL,
    creator_id INT NOT NULL,
	purchase_id INT NOT NULL,
	FOREIGN KEY (creator_id) REFERENCES creator(creator_id),
	FOREIGN KEY (purchase_id) REFERENCES purchase_info(purchase_id)
);



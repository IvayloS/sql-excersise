# sql-excersise

1) Which songs has Ashley produced in the database?
SQL: select title from media_info where creator_id in (select creator_id from creator where name = 'rimsa')

2) How many media items are purchased in total?
SQL: select count(id) from media_info

3) How many songs are purchased?
SQL: select count(id) from media_info where media_type = 'music'

4) How many movies are purchased?
SQL: select count(id) from media_info where media_type = 'movie'

5) What is the most expensive item that has been purchased?
SQL: select max(amount_paid) from purchase_info

6) What are the top 3 most expencive items?
SQL: select mi.title, pi.amount_paid from media_info mi
	left join purchase_info pi on mi.purchase_id = pi.purchase_id
	order by pi.amount_paid desc
	limit 3

7) How many different genres are purchased?
SQL: select distinct count(genre) from media_info

8) How many items are purchased online?
SQL: select count(purchase_id) from purchase_info where url is not null

9) How many items are purchased after 09/18/2019?
SQL: select count(purchase_id) from purchase_info where date_time > '09-18-2019'

10) How many songs are produced after the year 2000?
SQL: select count(id) from media_info where year > 2000
